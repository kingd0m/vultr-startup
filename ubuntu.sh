#!/bin/bash

set -e

tabs 4
clear

ROOT_UID=0
HOMEDIR=/home
SSH_PORT=4764
USERNAME=$1
PASSWORD=$2
USERNAME_UID=`shuf -i 1000-9999 -n 1`
SUDOERS_DEPLOYFILE="/etc/sudoers.d/automate-deploy"
SSHDIR=".ssh"
USER_SSH_DIR="$HOMEDIR/$USERNAME/$SSHDIR"

if [ $UID -ne $ROOT_UID ]
then                                                                                                       
    echo "You need to be ROOT to perform the script";
    exit 1
fi

##########################################
# Prepare local non root user
##########################################

echo "Add user $USERNAME"
useradd -u $USERNAME_UID --shell '/bin/bash' $USERNAME

echo "Set password for user $USERNAME"
echo "$USERNAME:$PASSWORD" | chpasswd

echo "Add User to sudo group"
usermod -aG sudo $USERNAME

echo "Create user SSH directory"
mkdir -p $USER_SSH_DIR
chmod 700 $USER_SSH_DIR

echo "Add authorization keys from ROOT to user $USERNAME"
mv /root/.ssh/authorized_keys "$USER_SSH_DIR/authorized_keys"
chmod 740 "$USER_SSH_DIR/authorized_keys"
chown -R $USERNAME:$USERNAME $USER_SSH_DIR

echo "Add user $USERNAME to sudoers file"
echo "$USERNAME	ALL = NOPASSWD: ALL" > $SUDOERS_DEPLOYFILE
visudo -c -f $SUDOERS_DEPLOYFILE

echo "Assign user Homedirectory to the user"
chown -R $USERNAME:$USERNAME $HOMEDIR/$USERNAME

##########################################
# Secure Host
##########################################

# Update SSHD Config
sed -e 's/#\?\(Port\s*\).*$/\1 4764/' -e 's/#\?\(PasswordAuthentication\s*\).*$/\1 no/' /etc/ssh/sshd_config > temp.txt 
mv -f temp.txt /etc/ssh/sshd_config

service ssh restart

##########################################
# Install additional Software
##########################################

# Install Docker
apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update

groupadd docker
usermod -aG docker $USERNAME

echo "Script has been executed successfully"