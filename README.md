How to execute the Script for user "TestUser"
```bash
#!/bin/bash
export TERM=xterm

export STARTUP_URL=https://bitbucket.org/kingd0m/vultr-startup/raw/master/ubuntu.sh

export STARTUP_LOCAL_FILE=ubuntu.sh

wget $STARTUP_URL --no-cache

chmod +x $STARTUP_LOCAL_FILE

source $STARTUP_LOCAL_FILE TestUser 12345
```

Manuell post installation steps to get docker running as it fails in combination with the startup script
```
apt-get -y install docker-ce
```